import Vue from 'vue';
import VueRouter from 'vue-router';
import IncidentListPage from '~/views/list.vue';
import IncidentDetailsPage from '~/views/details.vue';
import PageNotFound from '~/views/404.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'list',
    component: IncidentListPage,
  },
  {
    path: '/:link',
    name: 'details',
    component: IncidentDetailsPage,
  },
  { path: '*', component: PageNotFound },
];

const router = new VueRouter({
  routes,
});

export default router;
